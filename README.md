You need to develop one page using CSS (or SASS), HTML, Java Scripts/jQuery (if necessary) without any CMS.

In result we need:

* Clean CSS and HTML code
* Banner image and banner mask are in different blocks (layers) to make it easy to change banner image
* Mobile responsive page using media queries (without Bootstrap and other frameworks)
* Also specify how much time you have spent on this task.